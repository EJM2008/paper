import * as React from 'react';

import Login from './screens/Login';
import Autenticacao from './screens/Autenticacao';
import MainPage from './screens/MainPage';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function Routes() {

    const Stack = createStackNavigator();

    return (
        <NavigationContainer >
            <Stack.Navigator 
                screenOptions={{
                headerShown: false
                }}
                initialRouteName="login"
                >       
                <Stack.Screen name="login" component={Login} />
                <Stack.Screen name="autenticacao" component={Autenticacao} />
                <Stack.Screen name="page" component={MainPage} />
            </Stack.Navigator>
        
        </NavigationContainer>
    );
};

export default Routes;