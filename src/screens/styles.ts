
import * as React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
// Estilos da pagina Login
    page: {
      flexDirection: 'column',
      height: '100%',
      justifyContent: 'center',   
    },
    LinearGradient:{
      flex: 1,
    },
    preenchimento:{
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginBottom: 30,
    },
    visual:{
      justifyContent: 'center',
      alignSelf: 'center',
    },
    produto: {
      marginTop: (-0.06 * windowHeight),
      marginBottom: (0.06 * windowHeight),
      padding: 40,
      textAlign: 'center',
      color: 'orange',
      fontWeight: 'bold',
      fontSize: (0.07 * windowWidth),
    },
    logo: {
      height: (0.2 * windowHeight),
      width: (0.7 * windowWidth),
      resizeMode: 'stretch',
      marginLeft: 15,
    },
    
    box:{
      margin: 15,
    },
    botoes: {
      backgroundColor: 'gray',
      width: '70%',
      alignSelf: 'center',
    },

//Estilos da pagina Autenticacao
    senha:{    
      backgroundColor: 'gray',
      width:(0.23 * windowWidth),
      height: (0.11 * windowWidth),
    },
    autenticacao: {
      fontSize: 17,
      fontWeight: 'bold',
      marginTop: (-0.338 * windowHeight),
      textAlign: 'center',
      backgroundColor: '#13186e',
      color: '#ffffff',
      width: (0.4 * windowWidth),
    },
    viewTitle: {
      alignContent: "center", 
      justifyContent: "center",
      alignSelf: "center", 
    },
    viewCrypto: {
      alignContent: "center", 
      justifyContent: "center",
      alignSelf: "center", 
    },
    positions: {
      flexDirection: 'row',
    },
    crypto: {
      color: '#ffffff',
      marginTop: (-0.1 * windowHeight),
      padding: 0.5,
      fontSize: (0.04 * windowWidth),
    },
    crypto2: {
      color: '#ffffff',
      marginTop: (-0.102 * windowHeight),
      padding: 0.02,
      fontSize: (0.048 * windowWidth),
    },
    blocks: {  
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around',
      marginBottom: (0.02 * windowWidth),
      marginLeft: (0.045 * windowWidth),
      marginRight: (0.045 * windowWidth),
    },
    direction: {
      flexDirection: 'column',
      marginTop: (0.05 * windowWidth),
    },
    block2: {
      flexDirection: 'row',
      alignSelf: 'center',
      marginLeft: (0.045 * windowWidth),
      marginRight: (0.045 * windowWidth),
    },
    box2: {  
      position: 'absolute',   
      borderColor:'gray',
      borderWidth: (0.005 * windowWidth),
      height: (0.6 * windowHeight),
      width: (0.85 * windowWidth),
      flexDirection: 'column',
      justifyContent: 'center',    
    },
    page2: {
      height: windowHeight,
      alignItems: 'center',
      justifyContent: 'center',    
    },
    icon3: {
      fontSize: (0.06 * windowWidth),
      textAlign: 'center'
    },
    confirma: {
      backgroundColor: 'gray',
      width:(0.34 * windowWidth),
      height: (0.11 * windowWidth),
      margin: (0.02 * windowWidth),
    },

//Estilos da pagina Page
    cadastro:{
        flex: 1, 
        backgroundColor: '#ffffff',  
        color: '#000000',    
    },
    icon: {
      color: '#ffffff',
      fontSize: (0.04 * windowHeight),
      padding: 5,
    },
    icon2: {
      color: '#ffffff',
      fontSize: (0.05 * windowHeight),
      padding: 5,
    },
    slider: {
      width: '90%',
      height: (0.08 * windowHeight),
      alignSelf: 'center',
    },
    koma: {
      justifyContent: 'space-between',
      backgroundColor: '#13186e',
    },
    subs: {
      alignSelf: 'center',
      color: '#dae028',
      fontSize: 10,
    },
    head: {
      resizeMode: 'stretch',
      height: (0.08 * windowHeight),
      width: (0.28 * windowWidth), 
      marginBottom: -12,  
    },
    titulo: {
      fontWeight: 'bold',
      color: '#000000',
      marginTop: 10,
      alignSelf: 'center',
      fontSize: (0.025 * windowHeight),
    },
    subtitulo: {
      color: '#000000',
      marginTop: 20,
      alignSelf: 'center',
      fontSize: (0.02 * windowHeight),
    },
    entrada: {
      color: '#000000',
      marginTop: 20,
      marginLeft: (0.01 * windowHeight),
      paddingLeft: (0.01 * windowHeight),
    },
    valores: {
      backgroundColor: '#ffffff',
      color: '#000000',
      textDecorationColor: '#000000',
      height: (0.08 * windowHeight),
      width: '90%',
      alignSelf: 'center',
      margin: 15,
    },
    bem: {
      backgroundColor: '#ffffff',
      color: '#000000',
      textDecorationColor: '#000000',
      height: (0.08 * windowHeight),
      width: (0.5 * windowWidth),
    },
    block: {
      flexDirection: 'row',
      justifyContent: 'space-around',
    }, 
    parcelas: {
      backgroundColor: '#ffffff',
      color: '#000000',
      textDecorationColor: '#000000',
      height: (0.08 * windowHeight),
      width: (0.3 * windowWidth),
    },
    bloco1: {
      alignSelf: 'flex-start',
    },
    bloco2: {
      alignSelf: 'flex-start',
    }, 
    outras: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    ocultar: {
      marginRight: (0.08 * windowHeight),
      fontSize: 18,
    },
    info: {
      color: '#000000',
      margin: (0.012 * windowHeight),
      padding: (0.03 * windowHeight),
    },
    calcular: {
      color: '#ffffff',
      tintColor: '#ffffff',
      backgroundColor: '#787673',
      alignSelf: 'center',
      width: (0.8 * windowWidth),
    },
    table: {
      textDecorationColor: 'blue',
    }, 
    tabela: {
      alignSelf: 'center',
      width: (0.9 * windowWidth),
      borderColor: 'gray',
      borderWidth: (0.002 * windowWidth),
      borderRadius: (0.02 * windowWidth),
      marginTop: (0.03 * windowWidth),
    },
    nomeproduto: {
      alignSelf: 'center',
      backgroundColor: '#c7cdd1',
      width: (0.9 * windowWidth),     
    },
    linha: {
      color: '#000000',
      textDecorationColor: '#000000'
    },
    line: {
      color: 'red',
    },
    simulacao: {
      color: '#000000',
      marginLeft: (0.09 * windowWidth),
      marginTop: (0.03 * windowWidth),
    },
    pentagon:{
      fontSize: (0.054 * windowWidth),
      margin: (0.6 * windowWidth),
    },
    oferta: {
      alignSelf: 'center',
      textAlign:'center',
      color: '#000000',
      fontSize: (0.03 * windowWidth),
      borderColor: 'gray',
      borderWidth: (0.002 * windowWidth),
      borderRadius: (0.02 * windowWidth),
      width: (0.8 * windowWidth),
      padding: (0.06 * windowWidth),
      marginBottom: (0.04 * windowWidth),
    },
    baixar: {
      color: '#ee0000',
    },
    salvar: {
      color: '#ffffff',
      tintColor: '#ffffff',
      backgroundColor: '#787673',
      alignSelf: 'center',
      width: (0.8 * windowWidth),
      marginBottom: (0.1 * windowWidth),
    },

  });