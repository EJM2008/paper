import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Menus from './Menus';
import Page from './Page';
import Login from './Login';

const Drawer = createDrawerNavigator();

const MainPage = () => {
  
    return (
      <Drawer.Navigator drawerContent={() => <Menus />}>
        <Drawer.Screen name="page" component={Page} />
        <Drawer.Screen name="login" component={Login} />
      </Drawer.Navigator>
    );
  };

  export default MainPage;