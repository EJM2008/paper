import * as React from 'react';
import { styles } from './styles';
import {
  View,
  Image,
} from 'react-native';

import { 
    Button,  
    Paragraph,
    TextInput,
    Switch,
  } from 'react-native-paper';

import {maskCpf} from '../Formata';

import LinearGradient from 'react-native-linear-gradient';


function Login ({ navigation }) {
  
  const [text, setText] = React.useState<string>('');
  const [isSwitchOn, setIsSwitchOn] = React.useState(false);

  const onToggleSwitch = () => setIsSwitchOn(!isSwitchOn);

  return ( 
  <LinearGradient 
  colors={['#13186e', '#0000CD', '#0000FF']}
  start={{x:0.0, y:0.85}}
  end={{x:0.0, y:1.0}}
  style={styles.LinearGradient}
  >
    <View style={styles.page}>
      <View style={styles.visual}>
        
        <Image 
          style = {styles.logo}
          source = {require('./Images/KOMATSU_HEADER.jpg')}
          />

        <Paragraph style={styles.produto}>
          Nome do Produto
        </Paragraph>

      </View>

      <TextInput 
      mode='flat'
      label="CPF"
      value={text}
      onChangeText={(text) => {setText(maskCpf(text))}} 
      style={styles.box}     
      />

      <View style={styles.preenchimento}>

        <Paragraph>
          Lembrar meu CPF
        </Paragraph>

        <Switch 
        value={isSwitchOn} 
        onValueChange={onToggleSwitch} 
        />

      </View>

      <Button 
      style={styles.botoes} 
      onPress={
        () => {
          if(text.length === 14){navigation.push('autenticacao')}
          else {alert("Digite CPF válido")}
        }}
      >
        Entrar
      </Button>
      
    </View>
  </LinearGradient>
  );
};

export default Login;