import * as React from 'react';
import { styles } from './styles'
import { View, Text } from 'react-native';
import { Drawer, DefaultTheme, Avatar } from 'react-native-paper';

const themes = {
    ...DefaultTheme,
     colors: {
      primary: '#13186e',
      accent: 'blue',  
      text: '#000000', 
      surface: 'orange',
      background: '#FF0000',
      placeholder: '#000000',
      backdrops: '#000000',
    }
  }

function Menus() {

    const [active, setActive] = React.useState('');

    return (
    
      <Drawer.Section title="Menu" theme={themes}>
      <View style={{alignItems: 'center'}}>
        <Avatar.Image size={60} source={require('./Images/Usuario.png')} />
        <Text>Usuário</Text>
      </View>
        <Drawer.Item 
          label='Home' 
          active={active === 'first'}
          onPress={() => setActive('first')}
          theme={themes}
        />
        <Drawer.Item 
          label='Opções' 
          active={active === 'second'}
          onPress={() => setActive('second')}
          theme={themes}
        />
        <Drawer.Item 
          label='Logout'
          active={active === 'third'}
          onPress={() => setActive('third')}
          theme={themes}
        />
      </Drawer.Section>

    );
  }

  export default Menus;