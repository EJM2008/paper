import * as React from 'react';
import { styles } from './styles';
import {
  View,
  Image, 
  ScrollView, 
} from 'react-native';

import { 
  Button, 
  Text,
  Paragraph,
  Appbar,
  DefaultTheme,
  RadioButton,
  Divider,
  TextInput,
  Title,
} from 'react-native-paper';
import {maskRS} from '../Formata';

import Slider from '@react-native-community/slider';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();

function Page ({ navigation }) {
  const [valor, setValor] = React.useState('');
  const [bem, setBem] = React.useState('');
  const [parcelas, setParcelas] = React.useState('');
  const [check, setCheck] = React.useState<string>('first');
  const [entrada, setEntrada] = React.useState(0);
  const [visible, setVisible] = React.useState<boolean>(true);

  const produto = "<NOME DO PRODUTO>";

  //Tema da página
  const themes = {
    ...DefaultTheme,
     colors: {
      primary: '#13186e',
      accent: 'blue',  
      text: '#000000', 
      background: '#FF0000',
      placeholder: '#adadad',
      backdrops: '#000000',
      disabled: '#adadad',
    }
  }

  const theme2 = {
      colors: { 
        primary: "#000000", 
        accent: "#000000", 
        text: "#000000", 
        placeholder: '#adadad', 
        surface: '#000000',
        disabled: '#adadad',    
    }
  }

  return ( 
    <ScrollView>
      <View style={styles.cadastro} >
        <Appbar.Header 
          style={styles.koma}
          title= "Usuario"
          subtitle = "Usuario"        
          >

          <TouchableOpacity onPress = {() => navigation.openDrawer()}>
            <Octicons name="grabber" style={styles.icon2}/>
          </TouchableOpacity>

          <View>

            <TouchableOpacity onPress = {() => navigation.push('autenticacao')}>
              <Image
                source = {require('./Images/KOMATSU_HEADER.jpg')} 
                style = {styles.head}
              />
            </TouchableOpacity>

            <Text style={styles.subs}>Usuário</Text>

          </View>
          
          <Octicons name="settings" style={styles.icon}/>
         
        </Appbar.Header>

        <Title style={styles.titulo}>
          RESULTADO DA PRÉ-SIMULAÇÃO
        </Title>

        <Paragraph style={styles.subtitulo}>
          ALTERE SUAS CONDIÇÕES COMO PREFERIR
        </Paragraph>

        <Paragraph style={styles.entrada}>
          Entrada mínima: R${entrada},00
        </Paragraph>

        <Slider 
          style={styles.slider}
          minimumValue={0}
          maximumValue={1000}
          step={20}
          minimumTrackTintColor="#dae028"
          maximumTrackTintColor="#000000"
          onValueChange={entrada => setEntrada(entrada)}
        /> 
        <Paragraph style={styles.entrada}>Entrada</Paragraph>

        <TextInput 
          label="R$"
          value={valor}
          onChangeText={valor => setValor(maskRS(valor))} 
          style= {styles.valores} 
          theme={theme2}
        />

        <View style={styles.block}>
          <View>

            <Paragraph style={styles.entrada}>Valor do bem</Paragraph>

            <TextInput 
            label="R$"
            value={bem}
            onChangeText={bem => setBem(bem)} 
            style= {styles.bem}  
            theme={theme2}        
            />

          </View>
          <View style={styles.bloco2}>

            <Paragraph style={styles.entrada}>Parcelas</Paragraph>

            <TextInput 
            placeholder="Ex: 10"
            value={parcelas}
            onChangeText={parcelas => setParcelas(parcelas)} 
            style= {styles.parcelas} 
            theme={theme2}           
            />

          </View>
          
        </View>
        <View style={styles.outras}>
          <Text style={styles.info}>OUTRAS INFORMAÇÕES </Text>

          <TouchableOpacity onPress={() => setVisible(!visible)}>
            {visible ? <Octicons name="chevron-down" style={styles.ocultar}/> : <Octicons name="chevron-up" style={styles.ocultar}/>}
          </TouchableOpacity>

        </View>

        <Button style={styles.calcular} theme>CALCULAR</Button>

        {visible ? 
        <View>
          <View style = {styles.tabela}>
                 
          <Button style={styles.nomeproduto}> {produto}</Button>

          <RadioButton.Group onValueChange={check => setCheck(check)} value={check} style={styles.line}>  
            <RadioButton.Item label="99x de R$9.999,99" value='first' theme={themes}/>
            <Divider />
            <RadioButton.Item label="60x de R$19.999,99" value="second" theme={themes}/>
            <Divider />
            <RadioButton.Item label="48x de R$22.999,99" value="third" theme={themes}/>
            <Divider />
            <RadioButton.Item label="36x de R$25.999,99" value="fourth" theme={themes}/>
            <Divider />
            <RadioButton.Item label="24x de R$28.999,99" value="fifth" theme={themes}/>        
          </RadioButton.Group>

        </View>

        <Text style={styles.simulacao}>
          <MaterialCommunityIcons name="pentagon-outline" style={styles.pentagon}/>
           Resultado da simulação 
          <Text style={styles.baixar}> BAIXAR</Text> {"\n"}
        </Text>

        <Paragraph style={styles.oferta}>
          Mensagem de oferta direcionada ao cliente
        </Paragraph>
        </View>
        :
        <Text>{"\n"}</Text>}

        <Button style={styles.salvar} >
          SALVAR PRÉ-SIMULAÇÃO
        </Button>
       
      </View>
    </ScrollView>
  );
};

export default Page;
