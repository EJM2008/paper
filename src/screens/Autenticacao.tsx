import * as React from 'react';
import { styles } from './styles';
import {
  View,
  Text,
} from 'react-native';

import { 
    Button,  
    DefaultTheme,
} from 'react-native-paper';

import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';

import LinearGradient from 'react-native-linear-gradient';


function Autenticacao ({navigation}) {
    const themes = {
        ...DefaultTheme,
         color: {
          primary: '#ffffff',
          accent: '#ffffff',  
          text: '#000000', 
          surface: 'orange',
          background: '#FF0000',
          placeholder: '#000000',
          backdrops: '#000000',
        }
    }

    const [count, setCount] = React.useState(0);

  return ( 
    <LinearGradient 
        colors={['#13186e', '#0000CD', '#0000FF']}
        start={{x:0.0, y:0.85}}
        end={{x:0.0, y:1.0}}
        style={styles.LinearGradient}
        >
        <View style={styles.page2}>
            <View style={styles.box2}>
                <View style={styles.viewTitle} >
                    <Text style={styles.autenticacao}>AUTENTICAÇÃO</Text>
                </View>

                <View style={styles.viewCrypto}>
                    <View style={styles.positions}>
                    {count>0 ? <Entypo name="controller-record" style={styles.crypto2}/> : <Feather name="circle" style={styles.crypto}/>}
                    {count>1 ? <Entypo name="controller-record" style={styles.crypto2}/> : <Feather name="circle" style={styles.crypto}/>}
                    {count>2 ? <Entypo name="controller-record" style={styles.crypto2}/> : <Feather name="circle" style={styles.crypto}/>}
                    {count>3 ? <Entypo name="controller-record" style={styles.crypto2}/> : <Feather name="circle" style={styles.crypto}/>}
                    {count>4 ? <Entypo name="controller-record" style={styles.crypto2}/> : <Feather name="circle" style={styles.crypto}/>}
                    {count>5 ? <Entypo name="controller-record" style={styles.crypto2}/> : <Feather name="circle" style={styles.crypto}/>}
                    </View>
                </View>
                <View style={styles.blocks}>

                    <Button style={styles.senha} onPress={()=>{setCount(count+1)}}>
                        4 ou 9
                    </Button>
                    <Button style={styles.senha}  onPress={()=>{setCount(count+1)}}>
                        5 ou 6
                    </Button>
                    <Button style={styles.senha}  onPress={()=>{setCount(count+1)}}>
                        7 ou 8
                    </Button>
                    
                </View> 

                <View style={styles.blocks}>

                    <Button style={styles.senha} onPress={()=>{setCount(count+1)}} >
                        0 ou 1
                    </Button>
                    <Button style={styles.senha} onPress={()=>{setCount(count+1)}} >
                        2 ou 3
                    </Button>
                    <Button style={styles.senha} onPress={()=>{setCount(count-1)}} >
                        <Feather name="delete" style={styles.icon3}  />
                    </Button>
                    
                </View>

                <View style={styles.direction}>
                    <View style={styles.block2}>
                        <Button style={styles.confirma} onPress={() => navigation.push('login')}>
                            Voltar
                        </Button>
                        <Button style={styles.confirma} 
                        onPress={() => {
                            if(count>5){
                                navigation.push('page')
                            }
                            else {
                                alert("A senha deve conter 6 caracteres")
                            }
                        }}>
                            Entrar
                        </Button>
                    </View>
                </View>

            </View>  
        </View>
    </LinearGradient>
  );
};

export default Autenticacao;