import * as React from 'react';
import { DefaultTheme, Provider as PaperProvider, Portal, } from 'react-native-paper';

import Routes from './Routes'

function App () {
  
  const theme = {
    ...DefaultTheme,
    colors: {
        primary: '#ffffff',
        accent: '#000000',
        background: '#13186e',
        surface: '#f59d0f',
        text: '#ffffff',
        placeholder: '#ffffff',
        backdrops: '#ffffff',
    },
  };

  return ( 
    <PaperProvider theme={theme}>
      <Portal>
        <Routes />
      </Portal>
    </PaperProvider>
  );
};

export default App;
